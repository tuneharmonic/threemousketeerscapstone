﻿using Blog.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.UI.Models
{
    public class BlogCategory
    {
        public int CategoryId { get; set; }
        public int BlogPostId { get; set; }
        public Category Category  { get; set; }
        public BlogPost BlogPost { get; set; }
    }
}