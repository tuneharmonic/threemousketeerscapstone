﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.UI.Models
{
    public class StaticPage
    {
        public int StaticPageId { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }
}