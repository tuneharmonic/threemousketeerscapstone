﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.UI.Models
{
    public class StaticPage
    {
        [Key]
        public int StaticPageId { get; set; }

        [Key]
        public string UserId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }
}