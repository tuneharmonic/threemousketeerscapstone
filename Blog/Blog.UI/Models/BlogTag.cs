﻿using Blog.UI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.UI.Models
{
    public class BlogTag
    {
        [Key]
        public int BlogPostId { get; set; }

        [Key]
        public int TagId { get; set; }
        public Tag Tag { get; set; }
        public BlogPost BlogPost { get; set; }
    }
}