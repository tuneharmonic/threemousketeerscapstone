﻿using Blog.UI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog.UI.Models
{
    public class BlogPost
    {
        [Required]
        [Key]
        [Column(Order = 1)]
        public int BlogPostId { get; set; }


        public string Title { get; set; }
        public string Body { get; set; }
        public string Summary { get; set; }
        public DateTime DatePosted { get; set; }

       
      
        public int UserId { get; set; }
        public User User { get; set; }


        public List<BlogCategory> AssignedCategory { get; set; }
        public List<Tag>AssignedTags { get; set; }

    }
}